# Teclado Tamazight-Español
El presente repositorio contiene una distribución del teclado en la cual se añade al teclado estándar español los caracteres necesarios para escribir en Tamazight de forma más amena. Se ha seguido el alfabeto presentado en el excelente y pionero libro [Curso de Lengua Tamazight, Jahfar Hassan Yahia](https://archive.org/details/curso-de-lengua-tamazight-espanol-tamazight-nivel-elemental).

Actualmente solo esta disponible para sistemas linux, ya que no estoy familiarizado con las particularidades de Windows y MacOs.

## Guías
- [Linux](guías/linux.md)
- [Windows](guías/windows.md)

## ¿Cómo funciona?
La organización del teclado español se mantiene añadiendo lo siguente:

| Teclas a presionar | m | M |
|--------------------|---|---|
| ALT GR + G         | ġ | Ġ |
| ALT GR + R         | ṛ | Ṛ |
| ALT GR + H         | ḥ | Ḥ |
| ALT GR + S         | ṣ | Ṣ |
| ALT GR + Z         | ẓ | Ẓ |
| ALT GR + C         | ċ | Ċ |
| ALT GR + A         | ɛ | Σ |
| ALT GR + T         | ṭ | Ṭ |
| ALT GR + V         | ṯ | Ṯ |
| ALT GR + D         | ḍ | Ḍ |
| ALT GR + X         | ḏ | Ḏ |
