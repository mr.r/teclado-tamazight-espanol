Por desgracia este proceso requiere cierta familiarización con la terminal de linux.
Ha sido probada solo en Fedora, pero debería funcionar en cualquier otra distribución:

1. Descargue el archivo `mz` en el directorio `/usr/share/X11/xkb/symbols/`:
```
wget -O /usr/share/X11/xkb/symbols/mz https://gitlab.com/mr.r/teclado-tamazight-espanol/-/raw/main/resources/mz
```
2. Edite el archivo `/usr/share/X11/xkb/rules/evdev.xml`:
```
nano $EDITOR /usr/share/X11/xkb/rules/evdev.xml
```
3. Busque en el archivo `layoutList`
4. Añada el siguente fragmento tal y como se indica:
```xml
  <layoutList>

      <layout>
      <configItem>
        <name>mz</name>        
        <shortDescription>mz</shortDescription>
        <description>Tamazight (Tamazgha)</description>        
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>tamazight-melilla</name>
            <description>Tamazight (Teclado español)</description>
          </configItem>
        </variant>
      </variantList>
    </layout>

...
```

Tras cerrar y abrir sesión podrá ir a configuración y elegir la distribución
