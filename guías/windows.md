1. Descargue y descromprima [tamazight-español](https://gitlab.com/mr.r/teclado-tamazight-espanol/-/raw/main/resources/windows/teclado-espanol-tamazight.zip?inline=false)
2. Ejecute "setup.exe"
3. Le saldrá una ventana indicando que se ha instalado con éxito.
4. En su pantalla, en la parte inferior derecha verá un simbolo del mundo, al hacer click podrá elegir el teclado español-tamazight y usarlo.
